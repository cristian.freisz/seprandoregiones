package visual;

import grafosProv.*;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import visual.TestMap.MapPolyLine;

import javax.swing.JLabel;
import java.awt.Component;
import javax.swing.JList;

public class Mapaej {

	private JFrame frame;
	private JPanel panelMapa;
	private JPanel panelControles;
	private JPanel panelPesos;
	private JMapViewer _mapa;
	private JMapViewer _mapa2;
	private JMapViewer _mapa3;
	private ArrayList<Coordinate> provincias;
	private ArrayList<Coordinate> provincias2;
	ArrayList<MapPolyLine> Poligonos;
	MapPolygon polyline;
	private MapPolygonImpl _poligono;
	private JButton btnDibujarRegiones;
	private JTextField btnAgregarPeso;
	boolean bandera = false;
	ArrayList<Coordinate> _lasCoordenadas = new ArrayList<Coordinate>();
	private ArrayList<Set<Integer>> _vecinos;
	private int _vertice;
	Pais pais = new Pais();
	Mapa mapa;

	Coordinate valor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mapaej window = new Mapaej();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mapaej() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Pais paisConAristas = DatosArgentina.dameCiudades();
		mapa = new Mapa(paisConAristas);
		frame = new JFrame();
		frame.setBounds(100, 100, 725, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panelPesos = new JPanel();
		panelPesos.setBounds(0, 0, 676, 456);
		frame.getContentPane().add(panelPesos);

		panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 437, 446);
		frame.getContentPane().add(panelMapa);

		panelControles = new JPanel();
		panelControles.setBounds(457, 11, 242, 446);
		frame.getContentPane().add(panelControles);
		panelControles.setLayout(null);

		_mapa = new JMapViewer();
		_mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 4);

		_mapa2 = new JMapViewer();
		_mapa2.setDisplayPosition(new Coordinate(-34.521, -58.7008), 4);

		_mapa3 = new JMapViewer();
		_mapa3.setDisplayPosition(new Coordinate(-34.521, -58.7008), 4);

		panelMapa.add(_mapa);

		mapa.getPais();
		detectarCoordenadas();

		verLimitrofes(mapa.getPais());
		detectarPuntosProvincias();
		dibujarAGM();

		agregarPesos();
	}

	private void dibujarAGM() {

		btnDibujarRegiones = new JButton("Crear arbol generador minimo");
		btnDibujarRegiones.setBounds(10, 11, 195, 23);
		btnDibujarRegiones.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				bandera = true;
				btnDibujarRegiones.setEnabled(false);
				_mapa.setVisible(false);
				panelMapa.add(_mapa2);
				// Para que dibuje el arbol generador
				mapa.getPais().setLimitrofes(mapa.devolverArbolGenerador());
				// pais = mapa.pais;

				verLimitrofes2(mapa.getPais());
				dibujarRegions(mapa.getPais());
			}
		});
		panelControles.add(btnDibujarRegiones);
	}

	public void detectarCoordenadas()

	{
		provincias = new ArrayList<Coordinate>();
		Poligonos = new ArrayList<MapPolyLine>();
		provincias2 = new ArrayList<Coordinate>();

	}

	private void verLimitrofes(Pais p) {

		for (int i = 0; i < p.getLimitrofes().size(); i++) {

			double a = p.getLimitrofes().get(i).getOrigen().getCoordenada(); // .getOrigen().getCoordenada();
			double b = p.getLimitrofes().get(i).getOrigen().getcoord(); // .get(i).getOrigen().getcoord();
			Coordinate cordenada = new Coordinate(a, b);
			provincias.add(cordenada);

			double c = p.getLimitrofes().get(i).getDestino().getCoordenada(); // .destino.coordenada;
			double d = p.getLimitrofes().get(i).getDestino().getcoord(); // .destino.coord;
			Coordinate cordenada2 = new Coordinate(c, d);
			provincias.add(cordenada2);

			polyline = p.getLimitrofes().get(i).CrearPolyline();
			_mapa.addMapPolygon(p.getLimitrofes().get(i).CrearPolyline());
		}

	}

	private void verLimitrofes2(Pais p) {

		for (int i = 0; i < p.getLimitrofes().size(); i++) {

			double a = p.getLimitrofes().get(i).getOrigen().getCoordenada(); 
			double b = p.getLimitrofes().get(i).getOrigen().getcoord(); 
			Coordinate cordenada = new Coordinate(a, b);
			provincias.add(cordenada);

			double c = p.getLimitrofes().get(i).getDestino().getCoordenada(); 
			double d = p.getLimitrofes().get(i).getDestino().getcoord(); 
			Coordinate cordenada2 = new Coordinate(c, d);
			provincias.add(cordenada2);

			polyline = p.getLimitrofes().get(i).CrearPolyline();
			_mapa2.addMapPolygon(p.getLimitrofes().get(i).CrearPolyline());

		}

	}

	private void dibujarRegions(Pais p) {

		btnDibujarRegiones = new JButton("Crear Regiones");
		btnDibujarRegiones.setBounds(10, 64, 195, 23);
		btnDibujarRegiones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_mapa2.setVisible(false);
				panelMapa.add(_mapa3);
				String cantRegiones = JOptionPane.showInputDialog("cantidad de regiones: ");
				int cantReg = Integer.parseInt(cantRegiones);
				Pais regiones = mapa.regionesSeparadas(p, cantReg);

				for (int i = 0; i < regiones.getLimitrofes().size(); i++) {

					double a = p.getLimitrofes().get(i).getOrigen().getCoordenada();
					double b = p.getLimitrofes().get(i).getOrigen().getcoord(); 
					Coordinate cordenada = new Coordinate(a, b);
					provincias.add(cordenada);

					double c = p.getLimitrofes().get(i).getDestino().getCoordenada(); 
					double d = p.getLimitrofes().get(i).getDestino().getcoord();
					Coordinate cordenada2 = new Coordinate(c, d);
					provincias.add(cordenada2);

					polyline = regiones.getLimitrofes().get(i).CrearPolyline();

					_mapa3.addMapPolygon(polyline);

				}
			}
		});
		panelControles.add(btnDibujarRegiones);

	}

	private void detectarPuntosProvincias() {
		for (int i = 0; i < provincias.size(); i++) {
			MapMarker marker1 = new MapMarkerDot(provincias.get(i));
			marker1.getStyle().setColor(Color.black);
			marker1.getStyle().setBackColor(Color.blue);
			_mapa.addMapMarker(marker1);
			_mapa2.addMapMarker(marker1);
			_mapa3.addMapMarker(marker1);
		}

	};

	private void agregarPesos() {

		JButton btnNewButton = new JButton("Editar pesos");
		btnNewButton.setBounds(26, 304, 159, 23);
		btnNewButton.setFocusable(false);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setBorder(null);
		btnNewButton.setOpaque(false);

		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				mostrarPanel();
			}
		});

		panelControles.add(btnNewButton);
	}

	private void mostrarPanel() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					iniciarPanelPesos();
					panelMapa.setVisible(false);
					panelControles.setVisible(false);
					panelPesos.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	private void mostrarPanelMapa() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					
					panelMapa.setVisible(true);
					panelControles.setVisible(true);
					panelPesos.setVisible(false);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	private void iniciarPanelPesos() {

		JLabel lblNewLabel = new JLabel("Bs As - Cap");
		panelPesos.add(lblNewLabel);

		JTextField textField = new JTextField("0");
		panelPesos.add(textField);
		textField.setColumns(10);

		textField.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField.getText());
				mapa.getPais().getLimitrofes().get(0).setPeso(valor);

			}
		});

		JLabel lblNewLabel_1 = new JLabel("Bs As - Cordoba");
		panelPesos.add(lblNewLabel_1);

		JTextField textField_1 = new JTextField();
		panelPesos.add(textField_1);
		textField_1.setColumns(10);
		textField_1.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_1.getText());
				mapa.getPais().getLimitrofes().get(1).setPeso(valor);

			}
		});

		JLabel lblNewLabel_2 = new JLabel("Bs As - Entre rios");
		panelPesos.add(lblNewLabel_2);

		JTextField textField_2 = new JTextField();
		panelPesos.add(textField_2);
		textField_2.setColumns(10);
		textField_2.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_2.getText());
				mapa.getPais().getLimitrofes().get(2).setPeso(valor);

			}
		});

		JLabel lblNewLabel_3 = new JLabel("Bs As - La pampa");
		panelPesos.add(lblNewLabel_3);

		JTextField textField_3 = new JTextField();
		panelPesos.add(textField_3);
		textField_3.setColumns(10);
		textField_3.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_3.getText());
				mapa.getPais().getLimitrofes().get(3).setPeso(valor);

			}
		});

		JLabel lblNewLabel_4 = new JLabel("Bs As -Santa Fe");
		panelPesos.add(lblNewLabel_4);

		JTextField textField_4 = new JTextField();
		panelPesos.add(textField_4);
		textField_4.setColumns(10);
		textField_4.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_4.getText());
				mapa.getPais().getLimitrofes().get(4).setPeso(valor);

			}
		});

		JLabel lblNewLabel_6 = new JLabel("Catamarca - Cordoba");
		panelPesos.add(lblNewLabel_6);

		JTextField textField_5 = new JTextField();
		panelPesos.add(textField_5);
		textField_5.setColumns(10);
		textField_5.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_5.getText());
				mapa.getPais().getLimitrofes().get(5).setPeso(valor);

			}
		});

		JLabel lblNewLabel_7 = new JLabel("Catamarca - La rioja");
		panelPesos.add(lblNewLabel_7);

		JTextField textField_6 = new JTextField();
		panelPesos.add(textField_6);
		textField_6.setColumns(10);
		textField_6.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_6.getText());
				mapa.getPais().getLimitrofes().get(6).setPeso(valor);

			}
		});

		JLabel lblNewLabel_8 = new JLabel("Catamarca - Salta");
		panelPesos.add(lblNewLabel_8);

		JTextField textField_7 = new JTextField();
		panelPesos.add(textField_7);
		textField_7.setColumns(10);
		textField_7.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_7.getText());
				mapa.getPais().getLimitrofes().get(7).setPeso(valor);

			}
		});

		JLabel lblNewLabel_9 = new JLabel("Chaco - Corrientes");
		panelPesos.add(lblNewLabel_9);

		JTextField textField_8 = new JTextField();
		panelPesos.add(textField_8);
		textField_8.setColumns(10);

		textField_8.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_8.getText());
				mapa.getPais().getLimitrofes().get(8).setPeso(valor);

			}
		});

		JLabel lblNewLabel_10 = new JLabel("Chaco - Salta");
		panelPesos.add(lblNewLabel_10);

		JTextField textField_9 = new JTextField();
		panelPesos.add(textField_9);
		textField_9.setColumns(10);

		textField_9.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_9.getText());
				mapa.getPais().getLimitrofes().get(9).setPeso(valor);

			}
		});
		JLabel lblNewLabel_11 = new JLabel("Chaco - Santa Fe");
		panelPesos.add(lblNewLabel_11);

		JTextField textField_10 = new JTextField();
		panelPesos.add(textField_10);
		textField_10.setColumns(10);

		textField_10.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_10.getText());
				mapa.getPais().getLimitrofes().get(10).setPeso(valor);

			}
		});
		JLabel lblNewLabel_12 = new JLabel("Chaco - Santiago del estero");
		panelPesos.add(lblNewLabel_12);

		JTextField textField_11 = new JTextField();
		panelPesos.add(textField_11);
		textField_11.setColumns(10);

		textField_11.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_11.getText());
				mapa.getPais().getLimitrofes().get(11).setPeso(valor);

			}
		});

		JLabel lblNewLabel_13 = new JLabel("Chaco - Formosa");
		panelPesos.add(lblNewLabel_13);

		JTextField textField_12 = new JTextField();
		panelPesos.add(textField_12);
		textField_12.setColumns(10);

		textField_12.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_12.getText());
				mapa.getPais().getLimitrofes().get(12).setPeso(valor);

			}
		});

		JLabel lblNewLabel_14 = new JLabel("Chubut - Rio negro");
		panelPesos.add(lblNewLabel_14);

		JTextField textField_13 = new JTextField();
		panelPesos.add(textField_13);
		textField_13.setColumns(10);

		textField_13.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_12.getText());
				mapa.getPais().getLimitrofes().get(13).setPeso(valor);

			}
		});
		JLabel lblNewLabel_15 = new JLabel("Cordoba - la pampa");
		panelPesos.add(lblNewLabel_15);

		JTextField textField_14 = new JTextField();
		panelPesos.add(textField_14);
		textField_14.setColumns(10);

		textField_14.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_14.getText());
				mapa.getPais().getLimitrofes().get(14).setPeso(valor);

			}
		});
		JLabel lblNewLabel_16 = new JLabel("Cordoba - la rioja");
		panelPesos.add(lblNewLabel_16);

		JTextField textField_15 = new JTextField();
		panelPesos.add(textField_15);
		textField_15.setColumns(10);

		textField_15.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_15.getText());
				mapa.getPais().getLimitrofes().get(15).setPeso(valor);

			}
		});

		JLabel lblNewLabel_17 = new JLabel("Cordoba - Santa fe");
		panelPesos.add(lblNewLabel_17);

		JTextField textField_16 = new JTextField();
		panelPesos.add(textField_16);
		textField_16.setColumns(10);

		textField_16.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_16.getText());
				mapa.getPais().getLimitrofes().get(16).setPeso(valor);

			}
		});
		JLabel lblNewLabel_18 = new JLabel("Cordoba - Santiago del estero");
		panelPesos.add(lblNewLabel_18);

		JTextField textField_17 = new JTextField();
		panelPesos.add(textField_17);
		textField_17.setColumns(10);

		textField_17.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_17.getText());
				mapa.getPais().getLimitrofes().get(17).setPeso(valor);

			}
		});
		JLabel lblNewLabel_19 = new JLabel("Corrientes - entre rios");
		panelPesos.add(lblNewLabel_19);

		JTextField textField_18 = new JTextField();
		panelPesos.add(textField_18);
		textField_18.setColumns(10);

		textField_18.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_18.getText());
				mapa.getPais().getLimitrofes().get(18).setPeso(valor);

			}
		});
		JLabel lblNewLabel_20 = new JLabel("Corrientes - Santa fe");
		panelPesos.add(lblNewLabel_20);

		JTextField textField_19 = new JTextField();
		panelPesos.add(textField_19);
		textField_19.setColumns(10);

		textField_19.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_19.getText());
				mapa.getPais().getLimitrofes().get(19).setPeso(valor);

			}
		});
		JLabel lblNewLabel_21 = new JLabel("Entre rios - Santa fe");
		panelPesos.add(lblNewLabel_21);

		JTextField textField_20 = new JTextField();
		panelPesos.add(textField_20);
		textField_20.setColumns(10);

		textField_20.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_20.getText());
				mapa.getPais().getLimitrofes().get(20).setPeso(valor);

			}
		});
		JLabel lblNewLabel_22 = new JLabel("Formosa - salta");
		panelPesos.add(lblNewLabel_22);

		JTextField textField_21 = new JTextField();
		panelPesos.add(textField_21);
		textField_21.setColumns(10);

		textField_21.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_21.getText());
				mapa.getPais().getLimitrofes().get(21).setPeso(valor);

			}
		});
		JLabel lblNewLabel_23 = new JLabel("salta - Jujuy");
		panelPesos.add(lblNewLabel_23);

		JTextField textField_22 = new JTextField();
		panelPesos.add(textField_22);
		textField_22.setColumns(10);

		textField_22.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_22.getText());
				mapa.getPais().getLimitrofes().get(22).setPeso(valor);

			}
		});
		JLabel lblNewLabel_24 = new JLabel("La pampa - mendoza");
		panelPesos.add(lblNewLabel_24);

		JTextField textField_23 = new JTextField();
		panelPesos.add(textField_23);
		textField_23.setColumns(10);

		textField_23.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_23.getText());
				mapa.getPais().getLimitrofes().get(23).setPeso(valor);

			}
		});
		JLabel lblNewLabel_25 = new JLabel("La pampa - Neuquen");
		panelPesos.add(lblNewLabel_25);

		JTextField textField_24 = new JTextField();
		panelPesos.add(textField_24);
		textField_24.setColumns(10);

		textField_24.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_24.getText());
				mapa.getPais().getLimitrofes().get(24).setPeso(valor);

			}
		});
		JLabel lblNewLabel_26 = new JLabel("La pampa - Rio negro");
		panelPesos.add(lblNewLabel_26);

		JTextField textField_25 = new JTextField();
		panelPesos.add(textField_25);
		textField_25.setColumns(10);

		textField_25.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_25.getText());
				mapa.getPais().getLimitrofes().get(25).setPeso(valor);

			}
		});
		JLabel lblNewLabel_27 = new JLabel("La pampa - San luis");
		panelPesos.add(lblNewLabel_27);

		JTextField textField_26 = new JTextField();
		panelPesos.add(textField_26);
		textField_26.setColumns(10);

		textField_26.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_26.getText());
				mapa.getPais().getLimitrofes().get(26).setPeso(valor);

			}
		});
		JLabel lblNewLabel_28 = new JLabel("La rioja - San juan");
		panelPesos.add(lblNewLabel_28);

		JTextField textField_27 = new JTextField();
		panelPesos.add(textField_27);
		textField_27.setColumns(10);

		textField_27.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_27.getText());
				mapa.getPais().getLimitrofes().get(27).setPeso(valor);

			}
		});
		JLabel lblNewLabel_29 = new JLabel("La rioja - San Luis");
		panelPesos.add(lblNewLabel_29);

		JTextField textField_28 = new JTextField();
		panelPesos.add(textField_28);
		textField_28.setColumns(10);

		textField_28.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_28.getText());
				mapa.getPais().getLimitrofes().get(28).setPeso(valor);

			}
		});
		JLabel lblNewLabel_30 = new JLabel("Mendoza - Neuquen");
		panelPesos.add(lblNewLabel_30);

		JTextField textField_29 = new JTextField();
		panelPesos.add(textField_29);
		textField_29.setColumns(10);

		textField_29.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_29.getText());
				mapa.getPais().getLimitrofes().get(29).setPeso(valor);

			}
		});
		JLabel lblNewLabel_31 = new JLabel("Mendoza - Rio negro");
		panelPesos.add(lblNewLabel_31);

		JTextField textField_30 = new JTextField();
		panelPesos.add(textField_30);
		textField_30.setColumns(10);

		textField_30.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_30.getText());
				mapa.getPais().getLimitrofes().get(30).setPeso(valor);

			}
		});
		JLabel lblNewLabel_32 = new JLabel("Mendosa - San Luis");
		panelPesos.add(lblNewLabel_32);

		JTextField textField_31 = new JTextField();
		panelPesos.add(textField_31);
		textField_31.setColumns(10);

		textField_31.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_31.getText());
				mapa.getPais().getLimitrofes().get(31).setPeso(valor);

			}
		});
		JLabel lblNewLabel_33 = new JLabel("Mendosa - San juan");
		panelPesos.add(lblNewLabel_33);

		JTextField textField_32 = new JTextField();
		panelPesos.add(textField_32);
		textField_32.setColumns(10);

		textField_32.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_32.getText());
				mapa.getPais().getLimitrofes().get(32).setPeso(valor);

			}
		});
		JLabel lblNewLabel_34 = new JLabel("Corrientes - Misiones");
		panelPesos.add(lblNewLabel_34);

		JTextField textField_33 = new JTextField();
		panelPesos.add(textField_33);
		textField_33.setColumns(10);

		textField_33.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_33.getText());
				mapa.getPais().getLimitrofes().get(33).setPeso(valor);

			}
		});
		JLabel lblNewLabel_35 = new JLabel("Rio negro - Neuquen");
		panelPesos.add(lblNewLabel_35);

		JTextField textField_34 = new JTextField();
		panelPesos.add(textField_34);
		textField_34.setColumns(10);

		textField_34.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_34.getText());
				mapa.getPais().getLimitrofes().get(34).setPeso(valor);

			}
		});
		JLabel lblNewLabel_36 = new JLabel("Rio negro - Bs As");
		panelPesos.add(lblNewLabel_36);

		JTextField textField_35 = new JTextField();
		panelPesos.add(textField_35);
		textField_35.setColumns(10);

		textField_35.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_35.getText());
				mapa.getPais().getLimitrofes().get(35).setPeso(valor);

			}
		});
		JLabel lblNewLabel_37 = new JLabel("Santiago - Salta");
		panelPesos.add(lblNewLabel_37);

		JTextField textField_36 = new JTextField();
		panelPesos.add(textField_36);
		textField_36.setColumns(10);

		textField_36.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_36.getText());
				mapa.getPais().getLimitrofes().get(36).setPeso(valor);

			}
		});
		JLabel lblNewLabel_38 = new JLabel("Salta - Tucuman");
		panelPesos.add(lblNewLabel_38);

		JTextField textField_37 = new JTextField();
		panelPesos.add(textField_37);
		textField_37.setColumns(10);

		textField_37.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_37.getText());
				mapa.getPais().getLimitrofes().get(37).setPeso(valor);

			}
		});
		JLabel lblNewLabel_39 = new JLabel("San luis- San juan");
		panelPesos.add(lblNewLabel_39);

		JTextField textField_38 = new JTextField();
		panelPesos.add(textField_38);
		textField_38.setColumns(10);

		textField_38.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_38.getText());
				mapa.getPais().getLimitrofes().get(38).setPeso(valor);

			}
		});
		JLabel lblNewLabel_40 = new JLabel("Cordoba - San luis");
		panelPesos.add(lblNewLabel_40);

		JTextField textField_39 = new JTextField();
		panelPesos.add(textField_39);
		textField_39.setColumns(10);

		textField_39.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_39.getText());
				mapa.getPais().getLimitrofes().get(39).setPeso(valor);

			}
		});
		JLabel lblNewLabel_41 = new JLabel("Santa cruz - Chubut");
		panelPesos.add(lblNewLabel_41);

		JTextField textField_40 = new JTextField();
		panelPesos.add(textField_40);
		textField_40.setColumns(10);

		textField_40.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_40.getText());
				mapa.getPais().getLimitrofes().get(40).setPeso(valor);

			}
		});
		JLabel lblNewLabel_42 = new JLabel("Santa fe - Santiago");
		panelPesos.add(lblNewLabel_42);

		JTextField textField_41 = new JTextField();
		panelPesos.add(textField_41);
		textField_41.setColumns(10);

		textField_41.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_41.getText());
				mapa.getPais().getLimitrofes().get(41).setPeso(valor);

			}
		});
		JLabel lblNewLabel_43 = new JLabel("Santiago - Catamarca");
		panelPesos.add(lblNewLabel_43);

		JTextField textField_42 = new JTextField();
		panelPesos.add(textField_42);
		textField_42.setColumns(10);

		textField_42.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_42.getText());
				mapa.getPais().getLimitrofes().get(42).setPeso(valor);

			}
		});
		JLabel lblNewLabel_44 = new JLabel("Tierra del fuego - Santa cruz");
		panelPesos.add(lblNewLabel_44);

		JTextField textField_43 = new JTextField();
		panelPesos.add(textField_43);
		textField_43.setColumns(10);

		textField_43.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_43.getText());
				mapa.getPais().getLimitrofes().get(43).setPeso(valor);

			}
		});
		JLabel lblNewLabel_45 = new JLabel("Tucuman - Catamarca");
		panelPesos.add(lblNewLabel_45);

		JTextField textField_44 = new JTextField();
		panelPesos.add(textField_44);
		textField_44.setColumns(10);

		textField_44.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				double valor = Double.parseDouble(textField_44.getText());
				mapa.getPais().getLimitrofes().get(44).setPeso(valor);

			}
		});

		JButton btnNewButton = new JButton("Volver al mapa");
		btnNewButton.setBounds(26, 304, 159, 23);
		btnNewButton.setFocusable(false);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setBorder(null);
		btnNewButton.setOpaque(false);

		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				mostrarPanelMapa();
			}
		});

		panelPesos.add(btnNewButton);
	}

}