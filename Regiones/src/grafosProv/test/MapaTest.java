package grafosProv.test;

import static org.junit.Assert.*;

import org.junit.Test;

import grafosProv.Limitrofe;
import grafosProv.Mapa;
import grafosProv.Pais;
import grafosProv.Provincia;

public class MapaTest {

	@Test(expected = NullPointerException.class)
	public void crearMapaInstanciaNulaTest() {
		Mapa mapaNulo = new Mapa(null);
	}
	
	@Test(expected = NullPointerException.class)
	public void setPaisNuloTest() {
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		mapa.setPais(null);
	}

	@Test(expected = NullPointerException.class)
	public void regionesSeparadasNulasTest() {
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		mapa.regionesSeparadas(null, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void regionesSeparadasKNegativoTest() {
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		mapa.regionesSeparadas(pais, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void regionesSeparadasExcedidoRangoTest() {
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		mapa.regionesSeparadas(pais, 6);
	}
	
	@Test
	public void crearMapaConParametrosTest(){
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		
		assertTrue(pais.equals(mapa.getPais()));
	}
	
	@Test
	public void verificarMinimoAristasPrimTest() {
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		
		pais.setLimitrofes(mapa.devolverArbolGenerador());
		
		assertTrue((pais.getProvincias().size()-1)==pais.getLimitrofes().size());
	}
	
	@Test
	public void verificarAristaMasGrandeTest(){
		Pais pais = generarPais();
		Mapa mapa = new Mapa(pais);
		
		pais = mapa.regionesSeparadas(pais, 4);
		
		assertTrue(pais.getLimitrofes().size()==4);
	}
	

	private Pais generarPais() {
		Pais paisParametros = new Pais();
		Provincia buenosA = new Provincia("BuenosAires", -34.6157437, -58.5733832);
		Provincia capFederal = new Provincia("CapitalFederal" ,-34.6157437, -58.5733832);
		Provincia catamarca = new Provincia("Catamarca",-28.0215752, -66.3541337);
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		
		paisParametros.addProvincia(buenosA);
		paisParametros.addProvincia(capFederal);
		paisParametros.addProvincia(catamarca);
		paisParametros.addProvincia(chaco);
		paisParametros.addProvincia(chubut);
		
	      //Buenos Aires
		paisParametros.addLimitrofe(new Limitrofe(buenosA, capFederal, 30));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, catamarca, 10));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, chaco, 20));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, chubut, 340));
        //Capital
   
        //Catamarca
		paisParametros.addLimitrofe(new Limitrofe(catamarca, chaco, 300));
		paisParametros.addLimitrofe(new Limitrofe(catamarca, chubut, 400));
        
        //Chaco
		paisParametros.addLimitrofe(new Limitrofe(chaco, capFederal, 170));
		paisParametros.addLimitrofe(new Limitrofe(chaco, chubut, 19));
		
		return paisParametros;
	}
}
