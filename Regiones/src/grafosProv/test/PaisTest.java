package grafosProv.test;

import static org.junit.Assert.*;

import org.junit.Test;

import grafosProv.Limitrofe;
import grafosProv.Pais;
import grafosProv.Provincia;

public class PaisTest {
	
	//Si agregas una arista vacia deber�a crearla?
	@Test(expected = NullPointerException.class)
	public void agregarLimitrofeNuloTest() {
		Pais pais = generarPais();
		pais.addLimitrofe(null);
	}
	
	@Test(expected = NullPointerException.class)
	public void agregarLimitrofeSinOrigenTest() {
		
		Pais paisParametros = new Pais();
		
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		//Creamos Chubut
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		
		paisParametros.addProvincia(chaco);
		//Pero no agregamos a la lista de provincias
		paisParametros.addLimitrofe(new Limitrofe(chubut, chaco));
	}
	
	@Test(expected = NullPointerException.class)
	public void agregarLimitrofeSinDestinoTest() {
		Pais paisParametros = new Pais();
		
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		//Creamos Chubut
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		
		paisParametros.addProvincia(chaco);
		//Pero no agregamos a la lista de provincias
		paisParametros.addLimitrofe(new Limitrofe(chaco,chubut));
	}

	@Test
	public void addProvinciaTest() {
		Pais pais = new Pais();
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		pais.addProvincia(chaco);
		
		assertTrue("Chaco".equals(pais.getProvincias().get(0).getProvincia()));
	}
	
	@Test
	public void addLimitrofeTest() {
		Pais pais = new Pais();
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		pais.addProvincia(chubut);
		pais.addProvincia(chaco);
		
		pais.addLimitrofe(new Limitrofe(chaco, chubut));
		
		assertTrue(limiteExiste(pais.getLimitrofes().get(0)));
	}
	
	private boolean limiteExiste(Limitrofe limite) {
		return limite.getOrigen().getProvincia().equals("Chaco")&&limite.getDestino().getProvincia().equals("Chubut");
	}
	
	@Test
	public void construitPaisTest() {
		Pais pais = generarPais();
		
		Pais paisParametros = new Pais();
		Provincia buenosA = new Provincia("BuenosAires", -34.6157437, -58.5733832);
		Provincia capFederal = new Provincia("CapitalFederal" ,-34.6157437, -58.5733832);
		Provincia catamarca = new Provincia("Catamarca",-28.0215752, -66.3541337);
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		paisParametros.addProvincia(buenosA);
		paisParametros.addProvincia(capFederal);
		paisParametros.addProvincia(catamarca);
		paisParametros.addProvincia(chaco);
		paisParametros.addProvincia(chubut);
		
		assertTrue(provinciasIguales(pais, paisParametros));
	}
	
	private boolean provinciasIguales(Pais pais1, Pais pais2) {
		if(pais1.getProvincias().size()!=pais2.getProvincias().size())
			return false;
		boolean ret = false;
		for (int i = 0; i < pais1.getProvincias().size(); i++) {
				ret = pais1.getProvincias().get(i).getProvincia().equals(pais2.getProvincias().get(i).getProvincia());
		}

		return ret;
	}

	
	private Pais generarPais() {
		Pais paisParametros = new Pais();
		Provincia buenosA = new Provincia("BuenosAires", -34.6157437, -58.5733832);
		Provincia capFederal = new Provincia("CapitalFederal" ,-34.6157437, -58.5733832);
		Provincia catamarca = new Provincia("Catamarca",-28.0215752, -66.3541337);
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		
		paisParametros.addProvincia(buenosA);
		paisParametros.addProvincia(capFederal);
		paisParametros.addProvincia(catamarca);
		paisParametros.addProvincia(chaco);
		paisParametros.addProvincia(chubut);
		
	      //Buenos Aires
		paisParametros.addLimitrofe(new Limitrofe(buenosA, capFederal, 30));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, catamarca, 10));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, chaco, 20));
		paisParametros.addLimitrofe(new Limitrofe(buenosA, chubut, 340));
        //Capital
   
        //Catamarca
		paisParametros.addLimitrofe(new Limitrofe(catamarca, chaco, 300));
		paisParametros.addLimitrofe(new Limitrofe(catamarca, chubut, 400));
        
        //Chaco
		paisParametros.addLimitrofe(new Limitrofe(chaco, capFederal, 170));
		paisParametros.addLimitrofe(new Limitrofe(chaco, chubut, 19));
		
		return paisParametros;
	}
}
