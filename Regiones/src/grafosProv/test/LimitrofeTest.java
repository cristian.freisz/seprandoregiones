package grafosProv.test;

import static org.junit.Assert.*;
import grafosProv.Limitrofe;
import grafosProv.Provincia;
import java.io.IOException;

import org.junit.Test;

public class LimitrofeTest {
	
	Provincia p1, p2, p3, p4;
	Limitrofe l1, l2, l3, l4;
	
	@Test
	private void crearLimitrofes() {
		p1 = new Provincia("Tucumán", -26.8326885,-65.2926345);
		p2 = new Provincia ("Tierra del Fuego",-53.7865, -67.7105);
		p3 = new Provincia ("Buenos Aires", -34.6157437, -58.5733832);
		p4 = new Provincia ("CapitalFederal" ,-34.6157437, -58.5733832);
		
		l1 = new Limitrofe (p1, p2);
		l2 = new Limitrofe (p2, p3);
		l3 = new Limitrofe (p3, p1);
		l4 = new Limitrofe (p1, p4);
		
	}
	@Test
	public void testContiene() {
		assertTrue(l1.contiene(p1));
		assertTrue(l1.contiene(p2));
		assertFalse(l1.contiene(p3));
	}
	
	@Test
	public void testCompareTo() {
		assertTrue(l1.compareTo(l2) == 1);
		assertTrue(l2.compareTo(l3) == -1);
		assertTrue(l1.compareTo(l1) == 0);

	}


}
