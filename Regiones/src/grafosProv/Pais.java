package grafosProv;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Pais {

	private List<Provincia> provincias;
	private List<Limitrofe> limitrofes = new LinkedList<>();

	public void addProvincia(Provincia provincia) {
		if (provincias == null) {
			provincias = new LinkedList<>();
		}
		provincias.add(provincia);
	}

	public void setLimitrofes(List<Limitrofe> limitrofes) {
		this.limitrofes = limitrofes;
	}

	public List<Provincia> getProvincias() {
		return provincias;
	}

	public List<Limitrofe> getLimitrofes() {
		return limitrofes;
	}

	public void addLimitrofe(Limitrofe limitrofe) {
		if(limitrofe.getDestino()==null||limitrofe.getOrigen()==null)
			throw new NullPointerException("Debe ingresar una provincia distinta de null");
		provinciaPertenece(limitrofe.getDestino());
		provinciaPertenece(limitrofe.getOrigen());
		
		if (limitrofes == null) {
			limitrofes = new LinkedList<>();
		}
		limitrofes.add(limitrofe);
	}

	@Override
	public String toString() {
		return "Provincias= " + provincias;
	}
	
	private void provinciaPertenece(Provincia provincia) {
		if(!provincias.contains(provincia)) {
			throw new NullPointerException("Debe ingresar una provincia distinta de null");
		}
	}

}
