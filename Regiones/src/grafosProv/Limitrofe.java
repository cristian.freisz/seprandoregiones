package grafosProv;

import java.awt.Point;
import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import visual.TestMap.MapPolyLine;

public class Limitrofe implements Comparable<Limitrofe>, Cloneable {

	private Provincia origen;
	private Provincia destino;
	private double peso;
	private ArrayList<Coordinate> Coordenadas;
	MapPolygon linea;

	public Limitrofe(Provincia origen, Provincia destino, double peso) {
		this.origen = origen;
		this.destino = destino;
		this.peso = peso;
		Coordenadas = new ArrayList<Coordinate>();
	}

	public Limitrofe(Provincia origen, Provincia destino) {
		this.origen = origen;
		this.destino = destino;
	}

	public ArrayList<Coordinate> getCoordenadas() {
		return Coordenadas;
	}

	public void setCoordenadas(ArrayList<Coordinate> coordenadas) {
		Coordenadas = coordenadas;
	}

	public MapPolygon getLinea() {
		return linea;
	}

	public void setLinea(MapPolygon linea) {
		this.linea = linea;
	}

	public Provincia getOrigen() {
		return origen;
	}

	public void setOrigen(Provincia origen) {
		this.origen = origen;
	}

	public Provincia getDestino() {
		return destino;
	}

	public void setDestino(Provincia destino) {
		this.destino = destino;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "\n origen= " + origen.getProvincia() + ", destino=" + destino.getProvincia() + ", peso=" + peso + "]";
	}

	public MapPolygon CrearPolyline() {

		Coordenadas.add(new Coordinate(this.origen.getCoordenada(), this.origen.getcoord()));
		Coordenadas.add(new Coordinate(this.destino.getCoordenada(), this.destino.getcoord()));

		linea = new MapPolyLine(Coordenadas);

		return linea;
	}

	@Override
	public int compareTo(Limitrofe ari) {
		return peso > ari.getPeso() ? 1 : peso == ari.getPeso() ? 0 : -1;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		final Limitrofe newObj = new Limitrofe(origen, destino);
		newObj.setPeso(this.peso);
		return newObj;
	}

	public boolean contiene(Provincia provincia) {
		return origen.equals(provincia) || destino.equals(provincia);

	}
}
