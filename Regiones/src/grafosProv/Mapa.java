package grafosProv;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Mapa {

	private Pais paisMapa;
	private static List<Limitrofe> retLimitrofes;
	private List<Provincia> provinciasRecorridas;
	private List<Limitrofe> limitrofesClonados;

	public Mapa(Pais pais) {
		verificarNull(pais);
		this.paisMapa = pais;
		provinciasRecorridas = new LinkedList<Provincia>();
		retLimitrofes = new LinkedList<Limitrofe>();
		limitrofesClonados = new LinkedList<Limitrofe>();
		limitrofesClonados = (List<Limitrofe>) ((LinkedList<Limitrofe>) pais.getLimitrofes()).clone();

	}

	public Pais getPais() {
		return paisMapa;
	}

	public void setPais(Pais pais) {
		verificarNull(pais);
		this.paisMapa = pais;
	}

	/*devolverArbolGenerador()
	 * Output: List
	 * Devuelve un Arbol generador minimo a partir de las variables de clase del mapa
	 * 
	 */
	public List devolverArbolGenerador() {
		resolverArbolConPrim(limitrofesClonados); // Habria que cambiar para que funque con esto y no con el constructor
		return retLimitrofes;
	}

	private void resolverArbolConPrim(List<Limitrofe> limClonados) {
		if (!limClonados.isEmpty()) {
			Provincia l = limClonados.get(0).getOrigen();
			provinciasRecorridas.add(l);
		}

		while (!limClonados.isEmpty()) {
			Limitrofe ari = pesoMinimoSinCiclos(provinciasRecorridas);

			if (ari != null) {
				retLimitrofes.add(ari);

				if (provinciasRecorridas.contains(ari.getOrigen()))
					provinciasRecorridas.add(ari.getDestino());
				else
					provinciasRecorridas.add(ari.getOrigen());
			} else
				limClonados.clear(); // LIMPIO ARISTAS INUTILIZABLES
		}
	}

	private Limitrofe pesoMinimoSinCiclos(List<Provincia> lugaresRecorridos) {
		Limitrofe min = null;
		int indice = 0;

		for (int i = 0; i < lugaresRecorridos.size(); i++) {
			for (int j = 0; j < limitrofesClonados.size(); j++) {

				if (min == null && limitrofesClonados.get(j).contiene(lugaresRecorridos.get(i))) {
					if (!generaCiclo(lugaresRecorridos, limitrofesClonados.get(j)))
						min = limitrofesClonados.get(j);
				} else {
					if (min != null) {
						if (min.compareTo(limitrofesClonados.get(j)) == 1
								&& limitrofesClonados.get(j).contiene(lugaresRecorridos.get(i))) {

							if (!generaCiclo(lugaresRecorridos, limitrofesClonados.get(j))) {
								min = limitrofesClonados.get(j);
								indice = j;
							}
						}
					}
				}
			}
		}
		if (min != null)
			limitrofesClonados.remove(indice);

		return min;
	}

	private boolean generaCiclo(List<Provincia> l, Limitrofe arista) {
		return l.contains(arista.getOrigen()) && l.contains(arista.getDestino());
	}
	
	/*regionesSeparadas 
	 * Input: (Pais, int)
	 * Ouput: Pais
	 * Dando k numeros de regiones, va a devolver un país divido en las regiones solicitadas
	 */

	public Pais regionesSeparadas(Pais pais, int k) {

		if (k > pais.getProvincias().size() || k<0)
			throw new IllegalArgumentException(
					"El Rango debe ser entre (" + 0 + " ; " + pais.getProvincias().size() + ")");

		if (pais == null)
			throw new NullPointerException("Debe ingresar un pais con al menos un vértice");

		Pais regiones = new Pais();
		LinkedList<Provincia> provincias = (LinkedList<Provincia>) pais.getProvincias();
		// agrego las provincias
		for (int i = 0; i < provincias.size(); i++) {
			regiones.addProvincia(provincias.get(i));
		}

		LinkedList<Limitrofe> limitrofes = (LinkedList<Limitrofe>) pais.getLimitrofes();
		int j = k;
		// saco las k-1 aristas mï¿½s pesadas
		while ((j - 1) >= 0) {
			limitrofes.remove(aristaMasGrande(limitrofes));
			j--;
		}

		// agrego las demï¿½s aristas al nuevo grafo
		for (int i = 0; i < limitrofes.size(); i++) {
			regiones.addLimitrofe(limitrofes.get(i));
		}

		return regiones;
	}

	private static Limitrofe aristaMasGrande(LinkedList<Limitrofe> limitrofes) {

		Limitrofe masGrande = limitrofes.get(0);

		for (int i = 1; i < limitrofes.size(); i++) {
			if (limitrofes.get(i).getPeso() > masGrande.getPeso()) {
				masGrande = limitrofes.get(i);
			}
		}
		return masGrande;
	}

//Código de prueba de fallo
	private <T> void verificarNull(T objeto) {
		if (objeto == null) {
			throw new NullPointerException("Debe ingresar un pais con al menos un vï¿½rtice");
		}
	}

}
