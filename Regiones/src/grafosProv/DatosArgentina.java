package grafosProv;

public class DatosArgentina {
	
	public static Pais dameCiudades() {
		Pais pais1 = new Pais();
		 
		
		
		
		Provincia buenosA = new Provincia("BuenosAires", -34.6157437, -58.5733832);
		Provincia capFederal = new Provincia("CapitalFederal" ,-34.6157437, -58.5733832);
		Provincia catamarca = new Provincia("Catamarca",-28.0215752, -66.3541337);
		Provincia chaco = new Provincia("Chaco",-26.7908949, -60.471519);
		Provincia chubut = new Provincia("Chubut",-43.8849885, -68.4164489);
		Provincia cordoba = new Provincia("C�rdoba",-31.399084, -64.334431);
		Provincia corrientes = new Provincia("Corrientes",-29.1865113 ,-58.1043922);
		Provincia entreRios = new Provincia("Entre R�os",-33.008098, -58.5834522);
		Provincia formosa = new Provincia("Formosa",-26.18489, -58.17313);
		Provincia jujuy = new Provincia("Jujuy",-24.19457, -65.29712);
		Provincia laPampa = new Provincia("La Pampa",-36.6192291, -64.3712761);
		Provincia laRioja = new Provincia("La Rioja",-29.4142176,-66.8907967);
		Provincia mendoza = new Provincia("Mendoza",-32.8832582, -68.8935387);
		Provincia misiones = new Provincia("Misiones",-27.36708, -55.89608);
		Provincia neuquen = new Provincia("Neuqu�n",-38.9040134,-70.0799899);
		Provincia rioNegro = new Provincia("R�o Negro",-40.0893349,-66.9134617);
		Provincia salta = new Provincia("Salta",-24.7959127,-65.5006695);
		Provincia sanJuan = new Provincia("San Juan",-31.5316976,-68.5676963);
		Provincia sanLuis = new Provincia("San Luis",-33.2974938, -66.3797063);
		Provincia santaCruz = new Provincia("Santa Cruz",-48.7504194,-70.2646515);
		Provincia santaFe = new Provincia("Santa Fe",-30.5737494,-61.0380387);
		Provincia santiagoDelE = new Provincia("Santiago del Estero",-27.8015453,-64.3370891);
		Provincia tierraDelF = new Provincia("Tierra del Fuego",-53.7865, -67.7105);
		Provincia tucuman = new Provincia("Tucum�n",-26.8326885,-65.2926345);
        
        pais1.addProvincia(buenosA);
        pais1.addProvincia(capFederal);
        pais1.addProvincia(catamarca);
        pais1.addProvincia(chaco);
        pais1.addProvincia(chubut);
        pais1.addProvincia(cordoba);
        pais1.addProvincia(corrientes);
        pais1.addProvincia(entreRios);
        pais1.addProvincia(formosa);
        pais1.addProvincia(jujuy);
        pais1.addProvincia(laPampa);
        pais1.addProvincia(laRioja);
        pais1.addProvincia(mendoza);
        pais1.addProvincia(misiones);
        pais1.addProvincia(neuquen);
        pais1.addProvincia(rioNegro);
        pais1.addProvincia(salta);
        pais1.addProvincia(sanJuan);
        pais1.addProvincia(sanLuis);
        pais1.addProvincia(santaCruz);
        pais1.addProvincia(santaFe);
        pais1.addProvincia(santiagoDelE);
        pais1.addProvincia(tierraDelF);
        pais1.addProvincia(tucuman);        
     

      //Buenos Aires
        pais1.addLimitrofe(new Limitrofe(buenosA, capFederal, 30));
        pais1.addLimitrofe(new Limitrofe(buenosA, cordoba, 0));
        pais1.addLimitrofe(new Limitrofe(buenosA, entreRios, 0));
        pais1.addLimitrofe(new Limitrofe(laPampa,buenosA, 6));
        pais1.addLimitrofe(new Limitrofe(buenosA, santaFe, 340));
        //Capital
   
        //Catamarca
        pais1.addLimitrofe(new Limitrofe(catamarca, cordoba, 300));
        pais1.addLimitrofe(new Limitrofe(catamarca, laRioja, 400));
        pais1.addLimitrofe(new Limitrofe( salta, catamarca,1));
        
        //Chaco
        pais1.addLimitrofe(new Limitrofe(chaco, corrientes, 570));
        pais1.addLimitrofe(new Limitrofe(chaco, salta, 170));
        pais1.addLimitrofe(new Limitrofe( santaFe,chaco, 17));
        pais1.addLimitrofe(new Limitrofe(chaco, santiagoDelE, 19));
        pais1.addLimitrofe(new Limitrofe(chaco, formosa, 18));
        
        //Chubut
        pais1.addLimitrofe(new Limitrofe(chubut, rioNegro, 3));
        
        //Cordoba
        pais1.addLimitrofe(new Limitrofe(cordoba, laPampa, 640));
        pais1.addLimitrofe(new Limitrofe(cordoba, laRioja, 800));
        pais1.addLimitrofe(new Limitrofe( santaFe, cordoba,12));
        pais1.addLimitrofe(new Limitrofe(cordoba, santiagoDelE, 900));
        
        //Corrientes
        pais1.addLimitrofe(new Limitrofe(corrientes, entreRios, 340));
        pais1.addLimitrofe(new Limitrofe( santaFe, corrientes,10));
      
        //Entre Rios
        pais1.addLimitrofe(new Limitrofe(entreRios, santaFe, 9));
   
        //Formosa
        pais1.addLimitrofe(new Limitrofe(formosa, salta, 120));
        
        //Jujuy        
        pais1.addLimitrofe(new Limitrofe( salta,jujuy, 21));
        
        //La Pampa
        pais1.addLimitrofe(new Limitrofe(laPampa, mendoza, 230));
        pais1.addLimitrofe(new Limitrofe(laPampa, neuquen, 560));
        pais1.addLimitrofe(new Limitrofe( rioNegro, laPampa,5));
        pais1.addLimitrofe(new Limitrofe(laPampa, sanLuis, 470));
   
        //La Rioja
        pais1.addLimitrofe(new Limitrofe(laRioja, sanJuan, 120));
        pais1.addLimitrofe(new Limitrofe( sanLuis, laRioja,16));
    
        //Mendoza
        pais1.addLimitrofe(new Limitrofe(mendoza, neuquen, 160));
        pais1.addLimitrofe(new Limitrofe(mendoza, rioNegro, 360));
        pais1.addLimitrofe(new Limitrofe( sanLuis, mendoza,14));
        pais1.addLimitrofe(new Limitrofe(mendoza, sanJuan, 220));
       
        //Misiones        
        pais1.addLimitrofe(new Limitrofe(corrientes, misiones, 11));
        
        //Neuquen
        pais1.addLimitrofe(new Limitrofe( rioNegro,neuquen, 4));
        
        //Rio Negro
        pais1.addLimitrofe(new Limitrofe(rioNegro, buenosA, 400));
   
        //Salta
        pais1.addLimitrofe(new Limitrofe( santiagoDelE, salta,20));
        pais1.addLimitrofe(new Limitrofe(salta, tucuman, 22));
 
        ///San Juan
        pais1.addLimitrofe(new Limitrofe( sanLuis, sanJuan,15));
        
        //San Luis
        pais1.addLimitrofe(new Limitrofe( cordoba, sanLuis,13));
    
        //San Cruz        
        pais1.addLimitrofe(new Limitrofe(santaCruz, chubut, 2000));
        
        //Santa Fe
        pais1.addLimitrofe(new Limitrofe(santaFe, santiagoDelE, 180));
 
        //Santiago del Estero
        pais1.addLimitrofe(new Limitrofe(santiagoDelE, catamarca, 700));
    
        //Tierra del Fuego
        pais1.addLimitrofe(new Limitrofe(tierraDelF, santaCruz, 23));
       
        //Tucuman
        pais1.addLimitrofe(new Limitrofe(tucuman, catamarca, 210));
        
        return pais1;
    }

}
