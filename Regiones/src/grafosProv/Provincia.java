package grafosProv;

import java.util.ArrayList;
import java.util.List;

public class Provincia {

	private String provincia;
	private double coordenada;
	private double coord;

	public Provincia(String provincia, double coordenada, double coord) {
		this.provincia = provincia;
		this.coordenada = coordenada;
		this.coord = coord;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public void setCoordenada(double coordenada) {
		this.coordenada = coordenada;
	}

	public void setcoord(double coord) {
		this.coord = coord;
	}

	public double getCoordenada() {
		return coordenada;
	}

	public double getcoord() {
		return coord;
	}

	@Override
	public String toString() {
		return "\n \t Provincia= " + provincia;
	}
}
